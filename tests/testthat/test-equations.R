test_that("check creating equations", {

  expect_error(create_equations(""))
  # there are 2 warnings created by help, so adding default:
  expect_warning(create_equations(c("default", "help")))
  expect_warning(create_equations(NULL))

  nms <- names(create_equations("gm_fluorescence"))
  expect_setequal(nms , c("FLR.Cc", "FLR.gm", "gasanalyzer.UseFlags"))
  nms <- names(create_equations("Buck1981"))
  expect_setequal(nms , c("GasEx.SVPleaf", "GasEx.SVPcham",
                          "gasanalyzer.UseFlags"))
  nms <- names(create_equations("Buck1996"))
  expect_setequal(nms , c("GasEx.SVPleaf", "GasEx.SVPcham",
                          "gasanalyzer.UseFlags"))
  nms <- names(create_equations("GoffGratch1946"))
  expect_setequal(nms , c("GasEx.SVPleaf", "GasEx.SVPcham",
                          "gasanalyzer.UseFlags"))
  nms <- names(create_equations("ciras4"))
  expect_setequal(nms , c("SysObs.Instrument", "LeafQ.alpha",
                          "LeafQ.Conv", "gasanalyzer.UseFlags"))
  nms <- names(create_equations(c("gfs3000","gfs3000_light_bot")))
  expect_setequal(nms , c("SysObs.Instrument", "GasEx.gbw", "LeafQ.Qin",
                          "Meas.Tair", "gasanalyzer.UseFlags"))
  nms <- names(create_equations("li6400"))
  expect_setequal(nms , c("SysObs.Instrument", "GasEx.gbw", "GasEx.Rabs",
                          "GasEx.TairCnd", "LeafQ.alpha",
                          "gasanalyzer.UseFlags"))
  nms <- names(create_equations("li6400"))
  expect_setequal(nms , c("SysObs.Instrument", "GasEx.gbw", "GasEx.Rabs",
                  "GasEx.TairCnd", "LeafQ.alpha", "gasanalyzer.UseFlags"))
  nms1 <- names(create_equations(c("li6800" , "raw")))
  nms2 <- names(create_equations(c("li6800" , "O2_correction")))
  expected <-  c("SysObs.Instrument", "GasEx.gbw", "LeafQ.Qin", "LeafQ.alpha",
                 "LeafQ.Conv", "Leak.Fan", "Meas.H2Oa", "Meas.H2Or",
                 "Leak.CorrFact", "Meas.CO2a", "Meas.CO2r", "Meas.H2Os",
                 "Meas.CO2s", "GasEx.Ca", "gasanalyzer.UseFlags")
  expect_setequal(nms1, expected)
  expect_setequal(nms2, expected)
  expect_error(create_equations(c("li6800", "O2_correction","raw")))
  nms <- names(create_equations(c("d13C", "d13C_dis", "d13C_e_Busch2020")))
  expect_setequal(nms , c("d13C.xi", "d13C.ap", "d13C.ep", "d13C.t",
                         "d13C.Deltai", "d13C.Deltao", "d13C.DeltaiDeltao",
                         "d13C.A_pCa", "d13C.gm", "d13C.Cc",
                         "gasanalyzer.UseFlags"))

  nms <- names(create_equations(c("cuticular_conductance")))
  expect_setequal(nms , c("GasEx.gtw", "GasEx.gsw", "GasEx.gtc",
                          "GasEx.Ci", "gasanalyzer.UseFlags"))
  nms <- names(create_equations(c("boundary_conductance")))
  expect_setequal(nms ,  c("GasEx.gsw", "GasEx.Rabs", "GasEx.TairCnd",
                           "GasEx.Twall", "GasEx.SVPcham", "GasEx.Cpm",
                           "GasEx.lambda", "GasEx.TleafEB", "GasEx.TleafCnd",
                           "GasEx.SVPleaf", "GasEx.gtw", "GasEx.gbw",
                           "GasEx.gtc", "gasanalyzer.UseFlags"))

  # no point at testing more than this
  expect_named(calcs())
})

test_that("check equations vs 6800 xlsx", {
  Eqs <- create_equations("default")

  txtfile <- system.file("extdata", "lowo2", package = "gasanalyzer")
  # this xlsxfile has been loaded, recalculated and saved again to
  # provide an independent check of the imported calculations.
  xlsxfile <- system.file("extdata", "lowo2recalc.xlsx",
                          package = "gasanalyzer")

  #FIXME: I should disable the warning
  xlsxdata <- suppressWarnings(read_6800_xlsx(xlsxfile))
  xlsxdata_norecalc <- read_6800_xlsx(xlsxfile, recalculate = FALSE)
  xlsx_eqs <- read_6800_equations(xlsxfile)

  # there are a few equations we use that are not in the xlsx, add them
  # and check:
  nms <- c(names(xlsx_eqs), "SysObs.Instrument", "GasEx.TairCnd", "GasEx.Twall",
           "FLR.PhiQin_4", "GasEx.Cs",
           "gasanalyzer.UseFlags")
  expect_setequal(nms, c(names(Eqs), "SysConst.UseDynamic"))

  txtdata <- read_6800_txt(txtfile)

  # 1) For some reason, Li6800 Leak.Fan data differ slightly from the xlsx
  # 2) NA or NaN or Inf FLR variables get 0 in the txt
  check_txt_cols <- names(xlsxdata)[!names(xlsxdata) %in%
                                      c("gasanalyzer.Equations",
                                        "Leak.Fan", "FLR.Fv_Fm", "FLR.FopAlt",
                                        "FLR.Fop", "FLR.Fvp_Fmp", "FLR.qP",
                                        "FLR.qN", "FLR.qNFo", "FLR.qL",
                                        "FLR.1_qL", "SysObs.Filename")]
  # xlsx still incorrectly calculates some values, but only a few.
  check_xlsx_cols <- names(xlsxdata)[!names(xlsxdata) %in%
                                       c("gasanalyzer.Equations",
                                         "gasanalyzer.UseEqUnits",
                                         "FLR.qNFo")]

  expect_equal(xlsx_eqs, xlsxdata$gasanalyzer.Equations[[1]],
               tolerance = 1e-6, ignore_attr = TRUE)
  expect_equal(xlsxdata[check_xlsx_cols], xlsxdata_norecalc[check_xlsx_cols],
               tolerance = 1e-6, ignore_attr = TRUE)
  expect_equal(xlsxdata[check_txt_cols], txtdata[check_txt_cols],
               tolerance = 1e-6, ignore_attr = TRUE)

  # now see if our default calcs match licors:
  # the high tolerance is needed to less rounding in the default EB equations
  xlsxdata_default <- recalculate(xlsxdata, Eqs)
  expect_equal(xlsxdata[check_xlsx_cols], xlsxdata_default[check_xlsx_cols],
               tolerance = 0.005, ignore_attr = TRUE)
  # check li6800 specificsm only LeakFan is a bit different.
  xlsxdata_li6800 <- recalculate(xlsxdata, create_equations("li6800"))
  expect_equal(xlsxdata, xlsxdata_li6800,
               tolerance = 0.001, ignore_attr = TRUE)
})

test_that("check equations vs 6400 xlsx and xls", {

  txtfile <- system.file("extdata", "6400-testfile",
                         package = "gasanalyzer")
  xlsfile <- system.file("extdata", "6400-testfile.xls",
                         package = "gasanalyzer")
  # this xls file has been loaded and saved as xlsx
  xlsxfile <- system.file("extdata", "6400-testfile.xlsx",
                         package = "gasanalyzer")

  xls_data <- read_6400_xls(xlsfile)
  xlsx_data <- read_6400_xls(xlsxfile)
  xls_data_norecalc <- read_6400_xls(xlsfile, recalculate = FALSE)
  xlsx_data_norecalc <- read_6400_xls(xlsxfile, recalculate = FALSE)
  txtdata <- read_6400_txt(txtfile)


  expect_equal(xls_data, xlsx_data, tolerance = 1e-4, ignore_attr = TRUE)
  # the xlsx identifies NA correctly, but this messes up the comparison
  no_NA_cols <- !names(xlsx_data_norecalc) %in%
    c("FLR.Fv_Fm", "FLR.phiPS2", "FLR.ETR", "FLR.NPQ", "FLR.Fvp_Fmp", "FLR.qP",
      "FLR.qPFo", "FLR.qN", "FLR.qNFo")
  expect_equal(xls_data_norecalc[no_NA_cols], xlsx_data_norecalc[no_NA_cols],
               tolerance = 1e-4, ignore_attr = TRUE)
  # the txt contains actually fewer cols, and the tolerance needs to be
  # huge because it is rounding some things to 1 decimal
  all.equal(xls_data_norecalc[names(txtdata)], txtdata,
                           tolerance = 0.2, ignore_attr = TRUE)

  # default eq use light at Fs for phiCO2 calc, but 6400 uses actual
  check_xls_cols <- names(xls_data)[!names(xls_data) %in%
                                       c("gasanalyzer.Equations",
                                         "gasanalyzer.UseEqUnits",
                                         "FLR.phiCO2")]
  # also Fop is not yet set
  xls_data$FLR.Fop[xls_data$FLR.Fop==0] <- NaN
  xls_recalc_ge <- recalculate(xls_data_norecalc,
                               create_equations(c("default", "li6400")))

  expect_equal(xls_recalc_ge[check_xls_cols],xls_data[check_xls_cols],
               tolerance = 1e-4, ignore_attr = TRUE)
})

test_that("check custom and modifying equations", {
  Eqs <- create_equations("Buck1996",
                          GasEx.TairCnd = \() {1234},
                          GasEx.Twall = \() {GasEx.TairCnd  * 2}
                          )
  expect_setequal(names(Eqs) , c("GasEx.SVPleaf", "GasEx.SVPcham",
                                 "GasEx.TairCnd", "GasEx.Twall",
                                 "gasanalyzer.UseFlags"))

  system.file("extdata", "lowo2", package = "gasanalyzer") |>
    read_6800_txt() |>
    recalculate(Eqs) -> df
  expect_true(all(df$GasEx.TairCnd == 1234), TRUE)
  expect_true(all(df$GasEx.Twall == 2468), TRUE)

  Eqs <- modify_equations(Eqs,
                          GasEx.TairCnd = \() {256},
                          GasEx.Twall = \() {GasEx.TairCnd  / 16}
  )
  system.file("extdata", "lowo2", package = "gasanalyzer") |>
    read_6800_txt() |>
    recalculate(Eqs) -> df
  expect_true(all(df$GasEx.TairCnd == 256), TRUE)
  expect_true(all(df$GasEx.Twall == 16), TRUE)

  # TODO We might want to check equation order

})

