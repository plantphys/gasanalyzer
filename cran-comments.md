## Update to 0.4.3 (patch update)
  - update for an "Additional issue" problem:
      - using empty vectors with units may result in undefined behavior
      https://github.com/r-quantities/units/issues/380. This will be fixed
      in a new version of units, but I worked around the issue to be sure.
  - also fixed an another possible timestamp issue in the unit tests.
  - also added minor additional functionality and expanded the help.

## Test environments
* Developed on Gentoo Linux, R 4.4.2
* Tested on linux (R 4.4.2), R-devel in a gcc-UBSAN docker image and
 windows 11 (R 4.3.2).
* Used rhub::rc_submit() for R-* on Windows and Linux latest and clang-asan:
 - One annotation results from a warning in one of the examples. This was 
 always the case and is intended.
 - On MacOS all completes fine, but like last time it reports a pkg-config
 related error and warning. I think this is not related to the package.
* devtools::check_win_release() and devtools::check_mac_release() report
no issues.

## Reverse dependencies

None

## R CMD check results

0 errors | 0 warnings | 0 note

R CMD check succeeded
